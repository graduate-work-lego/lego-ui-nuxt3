import type { HiddenFieldsType } from "~/api/interfaces/IBlock";
import { BlocksType } from "~/api/mocks/blocks.mock";

export const hiddenBlocksLang: Record<keyof HiddenFieldsType, string> = {
  title: 'Заголовок',
  caption: 'Описание',
  button: 'Кнопка',
  mutedButton: 'Кнопка два',
  list: 'Список',
  "list.title": 'Заголовок списка',
  "list.caption": 'Описание списка',
  "list.subCaption": 'Второе описание списка',
  logoText: 'Логотип-текст',
};

export  const blocksTypeLang = {
  [BlocksType.HEADER]: "Заголовок",
  [BlocksType.MAIN]: "Обложка",
  [BlocksType.ADVANTAGES]: "Преимущества",
  [BlocksType.FAQ]: "Вопрос-ответ",
  [BlocksType.CONTACTS]: "Контакты",
};