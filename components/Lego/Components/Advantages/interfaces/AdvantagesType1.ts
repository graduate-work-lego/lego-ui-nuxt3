import type { IText } from "~/components/Lego/Components/interfaces";

export interface AdvantagesType1 {
  title: IText,
  list: IAdvantage[],
}

interface IAdvantage {
  title: IText,
  caption: IText,
  subCaption: IText,
}