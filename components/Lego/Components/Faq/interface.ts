import type { IText } from "~/components/Lego/Components/interfaces";

export interface FaqType1 {
  title: IText,
  list: IFaq[],
}

export interface FaqType2 extends FaqType1 {

}

interface IFaq {
  ask: IText,
  answer: IText,
}