import type { IButton, IText } from "~/components/Lego/Components/interfaces";

export interface MainType1 {
  title: IText,
  caption: IText,
  button: IButton,
  mutedButton: IButton,
}