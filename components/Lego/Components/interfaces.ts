export interface ILink {
  title: string,
  href: string,
  openInNewTab: boolean,
}

export interface IText {
  value: string;
  size: TextSizeEnum,
}

export interface ILogoText extends IText {
}

export interface IButton extends ILink {

}

export interface IIcon extends IText {
}

export enum TextSizeEnum {
  xl = "xl",
  h1 = "h1",
  h2 = "h2",
  h3 = "h3",
  h4 = "h4",
  h5 = "h5",
  p1 = "p1",
  p2 = "p2",
  p3 = "p3",
  p4 = "p4",
  p5 = "p5",
}