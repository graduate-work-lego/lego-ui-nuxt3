import type { IButton, ILink, ILogoText } from "~/components/Lego/Components/interfaces";

export interface HeaderType1 {
  list: ILink[];
  logoType: LogoType;
  logoText: ILogoText;
  button: IButton;
}

export interface HeaderType2 {
  list: ILink[];
  logoType: LogoType;
  logoText: ILogoText;
}

export enum LogoType {
  TEXT = "TEXT",
}