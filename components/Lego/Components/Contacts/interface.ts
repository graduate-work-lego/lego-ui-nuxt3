import type { IIcon, IText } from "~/components/Lego/Components/interfaces";

export interface IContactsType1 {
  title: IText,
  caption: IText,
  list: IContactListItem[],
}

interface IContactListItem {
  title: IText,
  caption: IText,
  icon: IIcon,
}

