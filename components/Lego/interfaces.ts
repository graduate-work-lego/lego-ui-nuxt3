export enum BlockActions {
  UP = "UP",
  DOWN = "DOWN",
  COPY = "COPY",
  VISIBLE_TOGGLE = "VISIBLE_TOGGLE",
  DELETE = "DELETE",
}

export enum AddButtonInsertPlaceType {
  TOP = "TOP",
  BOTTOM = "BOTTOM",
  CENTER = "CENTER",
}