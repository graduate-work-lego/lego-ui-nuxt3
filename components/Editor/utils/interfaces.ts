export enum ActionTypes {
  EDIT = 'EDIT',
  DESKTOP = 'DESKTOP',
  TABLET = 'TABLET',
  PHONE = 'PHONE'
}