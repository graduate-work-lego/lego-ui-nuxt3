import type { BackgroundType, IBlock } from "~/api/interfaces/IBlock";
import { TextSizeEnum } from "~/components/Lego/Components/interfaces";
import { ColorScheme } from "~/api/interfaces/IBlock";
import type { TextTheme } from "~/api/interfaces/common";
import { cloneDeepWith } from "lodash-es";
import { BASE_URL_STATIC } from "~/config";

export function getCopiedBlock(block: IBlock<any>) {
  return cloneDeepWith({ ...block, id: Date.now() });
}

export function getTextClassesBySize(size: TextSizeEnum): string {
  switch (size) {
    case TextSizeEnum.xl: {
      return "text-9xl font-bold";
    }
    case TextSizeEnum.h1: {
      return "text-6xl font-bold";
    }
    case TextSizeEnum.h2: {
      return "text-5xl font-semibold";
    }
    case TextSizeEnum.h3: {
      return "text-4xl font-medium";
    }
    case TextSizeEnum.h4: {
      return "text-3xl font-medium";
    }
    case TextSizeEnum.h5: {
      return "text-2xl font-normal";
    }
    case TextSizeEnum.p1: {
      return "text-xl font-normal";
    }
    case TextSizeEnum.p2: {
      return "text-lg font-normal";
    }
    case TextSizeEnum.p3: {
      return "text-base font-normal";
    }
    case TextSizeEnum.p4: {
      return "text-sm font-normal";
    }
    case TextSizeEnum.p5: {
      return "text-xs font-normal";
    }
  }
}

export function getBackgroundColor({ color, image }: BackgroundType) {
  if (image) {
    return {
      "background-image": `url(${BASE_URL_STATIC}/${image})`,
      "background-size": "cover",
      "background-repeat": "no-repeat",
    };
  }
  return { backgroundColor: color };
}

export function getColorByScheme(scheme: ColorScheme, theme: TextTheme = "default") {
  let color;
  if (theme === "default") {
    color = scheme === "WHITE" ? "#fff" : "#000";
  }
  if (theme === "gray") {
    color = scheme === "WHITE" ? "rgba(255, 255, 255, 0.56)" : "rgba(17, 17, 17, .56)";
  }
  if (theme === "additional") {
    color = scheme === "WHITE" ? "rgba(255, 255, 255, 0.56)" : "rgba(17, 17, 17, .24)";
  }
  return { color };
}