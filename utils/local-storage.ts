const ACCESS_TOKEN_KEY = "accessToken";
export const setJwt = (jwt: string) => {
  localStorage.setItem(ACCESS_TOKEN_KEY, jwt);
};
export const getJwt = (): string | null => {
  return localStorage.getItem(ACCESS_TOKEN_KEY);
};

export const removeJwt = () => {
  localStorage.removeItem(ACCESS_TOKEN_KEY);
};