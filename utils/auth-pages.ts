import { useAppStore } from "~/store/useAppStore";
import { useRouter } from "vue-router";

const authPagesUrls: string[] = [
  "sites",
  "profile",
];
const appStore = useAppStore();
const router = useRouter();
const route = useRoute();

export function checkAuthPage() {
  console.log(appStore);
  if (appStore.appLoading) {
    return;
  }

  if (authPagesUrls.includes(route.path.split("/")[1])) {
    router.push("/");
  }
}
