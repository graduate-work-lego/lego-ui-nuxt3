const GUARD_ROUTES: string[] = [
  "sites",
  "profile",
];

export function checkIsGuardRoute(route: { path: string }) {
  return GUARD_ROUTES.includes(route.path.split('/')[1])
}