import type { FetchOptions } from "ofetch";
import { BASE_URL } from "~/config";
import { $fetch } from "ofetch";
import type { IError } from "~/api/interfaces/Common/IError";
import { getJwt } from "#imports";

export const useApiFetch = async (url: string, {
  body,
  method,
  query,
}: FetchOptions<any>, withAuth: boolean = true): Promise<any> => {
  return $fetch(BASE_URL + url, {
    method,
    body,
    query,
    headers: {
      Authorization: withAuth ? `Bearer ${getJwt()}` : "",
    },
    onRequest: (context) => {
      if (withAuth) {
        context.options.credentials = "include";
      }
    },
  }).catch((e): IError => {
    throw {
      message: e.response._data.message,
      status: e.response.status,
    };
  });
};