interface IAuthLoginRequest {
  login: string;
  password: string;
}

interface IAuthRegisterRequest extends IAuthLoginRequest {
  repeatedPassword: string;
}
