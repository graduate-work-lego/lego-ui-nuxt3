export interface IAuthResponse {
  accessToken: string;
  user: any;
}