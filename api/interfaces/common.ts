import type { TextSizeEnum } from "~/components/Lego/Components/interfaces";

export type TextSizeListType = {
  id: TextSizeEnum,
  label: string,
}

export type TextTheme = 'default' | 'gray' | 'additional';