import type { IButton, ILink, IText } from "~/components/Lego/Components/interfaces";

export enum ElementsEnum {
  TEXT = "TEXT",
  LINK = "LINK",
  BUTTON = "BUTTON",
  ICON = 'ICON',
}

export type ElementsType = IText | ILink | IButton;