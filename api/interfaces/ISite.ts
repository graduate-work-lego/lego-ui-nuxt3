import type { IPage } from "~/api/interfaces/IPage";

export interface ISite {
  id: number;
  name: string;
  code: string;
  createdAt: string;
  pages: IPage[];
}