import type { IBlock } from "~/api/interfaces/IBlock";

export interface IPage {
  id: number;
  title: string;
  code: string;
  createdAt: string;
  blocks: IBlock<any>[]
}