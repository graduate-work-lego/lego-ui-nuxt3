import type { BlocksType } from "~/api/mocks/blocks.mock";

export interface IBlock<T> {
  id: number,
  type: BlocksType;
  subtype: string;
  settings: IBlockSettings;
  data: T;
  hiddenFields: HiddenFieldsType;
}

interface IBlockSettings {
  isHidden: boolean,
  background: BackgroundType,
}

export type HiddenFieldsType = {
  list?: boolean;
  'list.title'?: boolean;
  'list.caption'?: boolean;
  'list.subCaption'?: boolean;
  title?: boolean;
  caption?: boolean;
  logoText?: boolean;
  button?: boolean;
  mutedButton?: boolean;
}

export enum ColorScheme {
  WHITE = 'WHITE',
  BLACK = 'BLACK'
}
export type BackgroundType = {
  colorScheme: ColorScheme,
  color: string,
  image?: string,
}