const TIMEOUT_MS = 300;
export function returnMock<T>(data: T): Promise<T> {
  return new Promise((res) => {
    setTimeout(() => res(data), TIMEOUT_MS);
  })
}