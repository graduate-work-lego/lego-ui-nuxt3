import { useApiFetch } from "~/api/useApiFetch";

export class FileService {
  static uploadImageForSite(file: any, siteId: number): Promise<string> {
    const formData = new FormData();
    formData.append("file", file);
    return useApiFetch(`/files/upload-image`, {
      query: { siteId },
      body: formData,
      method: "POST",
    }).then(data => data.fileName);
  }
}