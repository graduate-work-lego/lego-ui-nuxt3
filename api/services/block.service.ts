import { useApiFetch } from "~/api/useApiFetch";
import type { IBlock } from "~/api/interfaces/IBlock";

export class BlockService {
  static updateBlock(block: IBlock<any>): Promise<IBlock<any>> {
    return useApiFetch(`/blocks`, {
      method: "PUT",
      body: block,
    });
  }

  static createBlock({ id, ...block }: IBlock<any>, pageId: number): Promise<IBlock<any>[]> {
    return useApiFetch(`/blocks/create/for-page/${pageId}`, {
      method: "POST",
      body: block,
    });
  }

  static removeBlock(blockId: number): Promise<boolean> {
    return useApiFetch(`/blocks/${blockId}`, {
      method: "DELETE",
    });
  }
}