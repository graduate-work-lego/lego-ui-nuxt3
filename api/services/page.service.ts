import { useApiFetch } from "~/api/useApiFetch";
import type { IPage } from "~/api/interfaces/IPage";

export class PageService {
  static getPageById(pageId: number): Promise<IPage> {
    return useApiFetch(`/pages/${pageId}`, {}, false);
  }

  static createPage(siteId: number): Promise<IPage> {
    return useApiFetch(`/pages/create`, {
      method: "POST",
      body: {
        siteId,
      },
    });
  }
}