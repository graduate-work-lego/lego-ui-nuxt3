import { useApiFetch } from "~/api/useApiFetch";
import type { ISite } from "~/api/interfaces/ISite";

export class SiteService {
  static getSites(): Promise<ISite[]> {
    return useApiFetch("/sites", {});
  }

  static getSiteById(siteId: number): Promise<ISite> {
    return useApiFetch(`/sites/${siteId}`, {}, false);
  }

  static createSite(): Promise<ISite> {
    return useApiFetch(`/sites/create`, {
      method: 'POST',
    });
  }
}