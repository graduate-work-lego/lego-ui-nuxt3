import { useApiFetch } from "~/api/useApiFetch";
import type { IAuthResponse } from "~/api/interfaces/Auth/IAuthResponse";

export class AuthService {
  static login(data: IAuthLoginRequest): Promise<IAuthResponse> {
    return useApiFetch("/auth/login", {
      body: data,
      method: "POST",
    });
  }

  static register(data: IAuthRegisterRequest): Promise<IAuthResponse> {
    return useApiFetch("/auth/register", {
      body: data,
      method: "POST",
    });
  }

  static logout() {
    return useApiFetch("/auth/logout", {
      method: "POST",
    });
  }

  static updateTokens(): Promise<IAuthResponse> {
    return useApiFetch("/auth/update-tokens", {
      method: "POST",
    });
  }
}
