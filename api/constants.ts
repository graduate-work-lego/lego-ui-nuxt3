import { TextSizeEnum } from "~/components/Lego/Components/interfaces";
import type { TextSizeListType } from "~/api/interfaces/common";
import type { BackgroundType } from "~/api/interfaces/IBlock";
import { ColorScheme } from "~/api/interfaces/IBlock";

export const TEXT_SIZES: TextSizeListType[] = [
  {
    id: TextSizeEnum.xl,
    label: "XL",
  },
  {
    id: TextSizeEnum.h1,
    label: "H1",
  },
  {
    id: TextSizeEnum.h2,
    label: "Заголовок H2",
  },
  {
    id: TextSizeEnum.h3,
    label: "Заголовок H3",
  },
  {
    id: TextSizeEnum.h4,
    label: "Заголовок H4",
  },
  {
    id: TextSizeEnum.h5,
    label: "Заголовок H5",
  },
  {
    id: TextSizeEnum.p1,
    label: "Текст p1",
  },
  {
    id: TextSizeEnum.p2,
    label: "Текст p2",
  },
  {
    id: TextSizeEnum.p3,
    label: "Текст p3",
  },
  {
    id: TextSizeEnum.p4,
    label: "Текст p4",
  },
  {
    id: TextSizeEnum.p5,
    label: "Текст p5",
  },
];

export const BACKGROUND_COLORS: BackgroundType[] = [
  {
    color: "#FFFFFF",
    colorScheme: ColorScheme.BLACK,
  },
  {
    color: "#111111",
    colorScheme: ColorScheme.WHITE,
  },
  {
    color: "#333333",
    colorScheme: ColorScheme.WHITE,
  },
  {
    color: "#303952",
    colorScheme: ColorScheme.WHITE,
  },
  {
    color: "#20BF6B",
    colorScheme: ColorScheme.WHITE,
  },
  {
    color: "#148bff",
    colorScheme: ColorScheme.WHITE,
  },
  {
    color: "#546DE5",
    colorScheme: ColorScheme.WHITE,
  },
  {
    color: "#FFDD2D",
    colorScheme: ColorScheme.BLACK,
  },
  {
    color: "#FFEBC1",
    colorScheme: ColorScheme.BLACK,
  },
  {
    color: "#F6F7F8",
    colorScheme: ColorScheme.BLACK,
  },
];