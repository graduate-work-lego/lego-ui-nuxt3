import type { HeaderType1 } from "~/components/Lego/Components/Header/interfaces/types";
import { LogoType } from "~/components/Lego/Components/Header/interfaces/types";
import { TextSizeEnum } from "~/components/Lego/Components/interfaces";

export const HeaderType1Data: HeaderType1 = {
  list: [
    {
      title: "О компании",
      href: "https://google.com",
      openInNewTab: true,
    },
    {
      title: "Услуги и цены",
      href: "https://google.com",
      openInNewTab: true,
    },
    {
      title: "Преимущества",
      href: "https://google.com",
      openInNewTab: true,
    },
  ],
  logoType: LogoType.TEXT,
  logoText: {
    value: "СтройДомВест",
    size: TextSizeEnum.h4,
  },
  button: {
    title: "Заказать",
    href: "https://google.com",
    openInNewTab: true,
  },
};