import { TextSizeEnum } from "~/components/Lego/Components/interfaces";
import type { FaqType1 } from "~/components/Lego/Components/Faq/interface";

export const FaqType1Mock: FaqType1 = {
  title: {
    value: "Вопросы и ответы",
    size: TextSizeEnum.h3,
  },
  list: [
    {
      ask: {
        value: "Как работает наш сервис?",
        size: TextSizeEnum.p1,
      },
      answer: {
        value: "Ежедневно только по Москве мы осуществляем порядка 50 переездов, в которых задействованы до 200 обученных сотрудников. У наших клиентов есть возможность получать услуги высокого качества.",
        size: TextSizeEnum.p4,
      },
    },
    {
      ask: {
        value: "Почему я не могу зарегистрироваться?",
        size: TextSizeEnum.p1,
      },
      answer: {
        value: "Ежедневно только по Москве мы осуществляем порядка 50 переездов, в которых задействованы до 200 обученных сотрудников. У наших клиентов есть возможность получать услуги высокого качества. ",
        size: TextSizeEnum.p4,
      },
    },
    {
      ask: {
        value: "Где я могу посмотреть ваш товар?",
        size: TextSizeEnum.p1,
      },
      answer: {
        value: "Ежедневно только по Москве мы осуществляем порядка 50 переездов, в которых задействованы до 200 обученных сотрудников. У наших клиентов есть возможность получать услуги высокого качества. ",
        size: TextSizeEnum.p4,
      },
    },
  ],
};