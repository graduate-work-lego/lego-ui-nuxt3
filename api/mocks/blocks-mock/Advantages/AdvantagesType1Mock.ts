import type { AdvantagesType1 } from "~/components/Lego/Components/Advantages/interfaces/AdvantagesType1";
import { TextSizeEnum } from "~/components/Lego/Components/interfaces";

export const AdvantagesType1Mock: AdvantagesType1 = {
  title: {
    value: "Наши преимущества",
    size: TextSizeEnum.h2,
  },
  list: [
    {
      title: {
        value: "24/7",
        size: TextSizeEnum.xl,
      },
      caption: {
        value: "Работаем круглосуточно и без выходных",
        size: TextSizeEnum.h5,
      },
      subCaption: {
        value: "Текст",
        size: TextSizeEnum.p4,
      },
    },
    {
      title: {
        value: "95",
        size: TextSizeEnum.xl,
      },
      caption: {
        value: "Успешно реализованных проектов",
        size: TextSizeEnum.h5,
      },
      subCaption: {
        value: "Текст",
        size: TextSizeEnum.p3,
      },
    },
    {
      title: {
        value: "740",
        size: TextSizeEnum.xl,
      },
      caption: {
        value: "Активных пользователей сервиса",
        size: TextSizeEnum.h5,
      },
      subCaption: {
        value: "Текст",
        size: TextSizeEnum.p3,
      },
    },
  ],
};