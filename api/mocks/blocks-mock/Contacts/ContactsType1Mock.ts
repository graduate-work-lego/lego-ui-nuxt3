import { TextSizeEnum } from "~/components/Lego/Components/interfaces";
import type { IContactsType1 } from "~/components/Lego/Components/Contacts/interface";

export const ContactsType1Mock: IContactsType1 = {
  title: {
    value: "Контакты",
    size: TextSizeEnum.h3,
  },
  caption: {
    value: "Метро «Авиамоторная», выход на пр-т Дзержинского. Далее до конечной на любом автобусе",
    size: TextSizeEnum.p2,
  },
  list: [
    {
      title: {
        value: "Адрес",
        size: TextSizeEnum.p1,
      },
      caption: {
        value: "Москва, ул. Красноказарменная, д. 14, БЦ «Омега», оф. 41",
        size: TextSizeEnum.p4,
      },
      icon: {
        value: "i-heroicons-map-pin",
        size: TextSizeEnum.h2,
      },
    },
    {
      title: {
        value: "Время работы",
        size: TextSizeEnum.p1,
      },
      caption: {
        value: "Пн—Пт: 10:00—19:00\n" +
          " Сб—Вс: выходные",
        size: TextSizeEnum.p4,
      },
      icon: {
        value: "i-heroicons-clock",
        size: TextSizeEnum.h2,
      },
    },
    {
      title: {
        value: "Как проехать",
        size: TextSizeEnum.p1,
      },
      caption: {
        value: "Метро «Авиамоторная», выход на пр-т Дзержинского, из перехода направо до остановки «ПКиО „Березовая роща“». Далее 2 остановки на любом автобусе",
        size: TextSizeEnum.p4,
      },
      icon: {
        value: "i-heroicons-paper-airplane",
        size: TextSizeEnum.h2,
      },
    },
    {
      title: {
        value: "Свяжитесь с нами",
        size: TextSizeEnum.p1,
      },
      caption: {
        value: "8 800 800 80 80\n" +
          "   info@pereezd21.ru",
        size: TextSizeEnum.p4,
      },
      icon: {
        value: "i-heroicons-phone",
        size: TextSizeEnum.h2,
      },
    },
  ],
};