import type { MainType1 } from "~/components/Lego/Components/Main/types";
import { TextSizeEnum } from "~/components/Lego/Components/interfaces";

export const MainType1Mock: MainType1 = {
  title: {
    value: "Квартира станет как новенькая",
    size: TextSizeEnum.h2,
  },
  caption: {
    value: "Разработаем дизайн-проект ремонта, согласуем перепланировку, привезем материалы. После ремонта вывезем мусор и проведем влажную уборку",
    size: TextSizeEnum.p1,
  },
  button: {
    title: 'Оставить заявку',
    href: 'https://google.com',
    openInNewTab: true,
  },
  mutedButton: {
    title: 'Узнать подробности',
    href: 'https://google.com',
    openInNewTab: true,
  },
};