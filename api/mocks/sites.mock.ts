import { returnMock } from "~/api/base-mock";
import type { ISite } from "~/api/interfaces/ISite";
import { pagesListMock } from "~/api/mocks/pages.mock";

export const sitesListMock: ISite[] = [
  {
    id: 1,
    name: "Лэндинг 1",
    code: "stdsadg",
    createdAt: "2024-03-09T17:49:56+00:00",
    pages: pagesListMock,
  },
];

export const siteMock: ISite = sitesListMock[0];

export function getSites(): Promise<ISite[]> {
  return returnMock<ISite[]>(sitesListMock);
}

export function getSiteById(siteId: string) {
  console.log(siteId);
  return returnMock<ISite>(siteMock);
}