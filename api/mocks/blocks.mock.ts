import { returnMock } from "~/api/base-mock";
import type { IBlock } from "~/api/interfaces/IBlock";
import { HeaderType1Data } from "~/api/mocks/blocks-mock/Header/HeaderType1Mock";
import { AdvantagesType1Mock } from "~/api/mocks/blocks-mock/Advantages/AdvantagesType1Mock";
import { cloneDeepWith } from "lodash-es";
import { BACKGROUND_COLORS } from "~/api/constants";
import { FaqType1Mock } from "~/api/mocks/blocks-mock/Faq/FaqType1Mock";
import { FaqType2Mock } from "~/api/mocks/blocks-mock/Faq/FaqType2Mock";
import { ContactsType1Mock } from "~/api/mocks/blocks-mock/Contacts/ContactsType1Mock";
import { HeaderType2Data } from "~/api/mocks/blocks-mock/Header/HeaderType2Mock";
import { MainType1Mock } from "~/api/mocks/blocks-mock/Main/MainType1Mock";

export enum BlocksType {
  HEADER = "header",
  MAIN = "main",
  ADVANTAGES = "advantages",
  FAQ = "faq",
  CONTACTS = "contacts"
}

export const blocks: IBlock<any>[] = [
  {
    id: 228,
    type: BlocksType.HEADER,
    subtype: "type-1",
    data: cloneDeepWith(HeaderType1Data),
    settings: {
      isHidden: false,
      background: BACKGROUND_COLORS[0],
    },
    hiddenFields: {
      button: false,
      logoText: false,
      list: false,
    },
  },
  {
    id: 14152,
    type: BlocksType.HEADER,
    subtype: "type-2",
    data: cloneDeepWith(HeaderType2Data),
    settings: {
      isHidden: false,
      background: BACKGROUND_COLORS[0],
    },
    hiddenFields: {
      logoText: false,
      list: false,
    },
  },
  {
    id: 1512152,
    type: BlocksType.MAIN,
    subtype: "type-1",
    data: cloneDeepWith(MainType1Mock),
    settings: {
      isHidden: false,
      background: {
        ...BACKGROUND_COLORS[1], image: "common/main-type1.jpeg",
      },
    },
    hiddenFields: {
      title: false,
      button: false,
      mutedButton: false,
      caption: false,
    },
  },
  {
    id: 229,
    type: BlocksType.ADVANTAGES,
    subtype: "type-1",
    data: cloneDeepWith(AdvantagesType1Mock),
    settings: {
      isHidden: false,
      background: BACKGROUND_COLORS[0],
    },
    hiddenFields: {
      title: false,
      list: false,
      "list.title": false,
      "list.caption": false,
      "list.subCaption": true,
    },
  },
  {
    id: 230,
    type: BlocksType.FAQ,
    subtype: "type-1",
    data: cloneDeepWith(FaqType1Mock),
    settings: {
      isHidden: false,
      background: BACKGROUND_COLORS[0],
    },
    hiddenFields: {
      title: false,
    },
  },
  {
    id: 231,
    type: BlocksType.FAQ,
    subtype: "type-2",
    data: cloneDeepWith(FaqType2Mock),
    settings: {
      isHidden: false,
      background: BACKGROUND_COLORS[0],
    },
    hiddenFields: {
      title: false,
    },
  },
  {
    id: 232,
    type: BlocksType.CONTACTS,
    subtype: "type-1",
    data: cloneDeepWith(ContactsType1Mock),
    settings: {
      isHidden: false,
      background: BACKGROUND_COLORS[0],
    },
    hiddenFields: {
      title: false,
      caption: false,
      "list.caption": false,
    },
  },
];

export function getBlocksByPageId(pageId: string): Promise<IBlock<any>[]> {
  return returnMock<IBlock<any>[]>(blocks.filter((item, i) => true));
}

export function getAllBlocks(): Promise<IBlock<any>[]> {
  return returnMock<IBlock<any>[]>(blocks.map(item => ({ ...item, data: cloneDeepWith(item.data) })));
}