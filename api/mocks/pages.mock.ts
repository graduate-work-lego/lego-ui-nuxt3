import { returnMock } from "~/api/base-mock";
import type { IPage } from "~/api/interfaces/IPage";

export const pagesListMock: IPage[] = [
  {
    id: "123",
    title: "Главная",
    code: "stdsadg.ru",
    blocks: [],
    createdAt: "2024-03-09T17:49:56+00:00",
  },
];

export function getPages(): Promise<IPage[]> {
  return returnMock<IPage[]>(pagesListMock);
}