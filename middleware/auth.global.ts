import { defineNuxtRouteMiddleware, navigateTo } from "#app";
import { useAppStore } from "~/store/useAppStore";
import { checkIsGuardRoute } from "~/utils/guard-routes";

export default defineNuxtRouteMiddleware((to, from) => {
  const appStore = useAppStore();
  if (appStore.appLoading) {
    return;
  }
  if (checkIsGuardRoute(to) && !appStore.user) {
    return navigateTo("/");
  }
});