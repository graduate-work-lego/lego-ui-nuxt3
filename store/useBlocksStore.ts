import { defineStore } from "pinia";
import type { IBlock } from "~/api/interfaces/IBlock";
import { PageService } from "~/api/services/page.service";
import { BlockService } from "~/api/services/block.service";
import type { IPage } from "~/api/interfaces/IPage";
import type { ISite } from "~/api/interfaces/ISite";

export interface IBlocksState {
  blocks: IBlock<any>[],
  page: IPage | null,
  site: ISite | null,
  editDisabled: boolean,
}

export const useBlocksStore = defineStore("blocks", {
  state: (): IBlocksState => ({
    site: null,
    blocks: [],
    page: null,
    editDisabled: false,
  }),
  getters: {
    resultBlocks: (state: IBlocksState) => state.blocks.filter(block => !block.settings.isHidden),
  },
  actions: {
    async getBlocks(pageId: number) {
      try {
        const page = await PageService.getPageById(pageId);
        this.page = page;
        this.blocks = page.blocks;
      } catch (e) {
        this.blocks = [];
        console.error(e);
      }
    },
    async updateBlock<T>(block: IBlock<T>): Promise<void> {
      try {
        const savedBlock = await BlockService.updateBlock(block);
        const savedBlockIndex = this.blocks.findIndex(item => item.id === savedBlock.id);
        this.blocks[savedBlockIndex] = savedBlock;
      } catch (e) {
        console.error(e);
      }
    },
    async createBlock<T>(block: IBlock<T>): Promise<void> {
      try {
        this.blocks = await BlockService.createBlock(block, this.page!.id);
      } catch (e) {
        console.error(e);
      }
    },
    async removeBlock<T>(block: IBlock<T>): Promise<void> {
      try {
        this.blocks = this.blocks.filter(item => item.id !== block.id)
        await BlockService.removeBlock(block.id);
      } catch (e) {
        console.error(e);
      }
    },
  },
});