import { defineStore } from "pinia";
import { AddButtonInsertPlaceType } from "~/components/Lego/interfaces";
import type { IBlock } from "~/api/interfaces/IBlock";

interface IBlocksGalleryState {
  isOpened: boolean,
  insertPlace: AddButtonInsertPlaceType | null,
  block: IBlock<any> | null,
}
export const useGalleryStore = defineStore('blocksGallery', {
  state: (): IBlocksGalleryState => ({
    isOpened: false,
    insertPlace: null,
    block: null,
  }),
  actions: {
    openGallery(insertPlace: AddButtonInsertPlaceType, block: IBlock<any> | null) {
      this.isOpened = true;
      this.insertPlace = insertPlace;
      this.block = block;
    },
    closeGallery() {
      this.isOpened = false;
      this.insertPlace = null;
      this.block = null;
    }
  },
})
