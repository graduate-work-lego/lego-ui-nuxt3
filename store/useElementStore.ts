import { defineStore } from "pinia";
import type { IBlock } from "~/api/interfaces/IBlock";
import type { ElementsEnum, ElementsType } from "~/api/interfaces/ElementsEnum";

export interface IElementState {
  isOpened: boolean;
  elementType: ElementsEnum | null;
  element: ElementsType | null,
  block: IBlock<any> | null,
}

export const useElementStore = defineStore("elementStore", {
  state: (): IElementState => ({
    isOpened: false,
    elementType: null,
    element: null,
    block: null,
  }),
  actions: {
    open(block: IBlock<any> | null, element: ElementsType, elementType: ElementsEnum) {
      this.isOpened = true;
      this.block = block;
      this.element = element;
      this.elementType = elementType;
    },
    close() {
      this.isOpened = false;
      this.element = null;
      this.elementType = null;
      this.block = null;
    },
  },
});