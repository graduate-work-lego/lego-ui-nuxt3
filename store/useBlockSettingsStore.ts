import { defineStore } from "pinia";
import type { IBlock } from "~/api/interfaces/IBlock";

interface IBlockSettingsState {
  isOpened: boolean,
  block: IBlock<any> | null,
  type: EditorSettingsType | null,
}

export const useBlockSettingsStore = defineStore("blockSettingsStore", {
  state: (): IBlockSettingsState => ({
    isOpened: false,
    block: null,
    type: null,
  }),
  actions: {
    openSettings(block: IBlock<any>, type: EditorSettingsType) {
      this.isOpened = true;
      this.block = block;
      this.type = type;
    },
    closeSettings() {
      this.isOpened = false;
      this.block = null;
    },
  },
});

export type EditorSettingsType = "STRUCTURE" | "BACKGROUND"
