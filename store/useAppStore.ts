import { defineStore } from "pinia";
import type { IUser } from "~/api/interfaces/User/IUser";
import { removeJwt, setJwt } from "~/utils/local-storage";
import { AuthService } from "~/api/services/auth.service";

interface IAppState {
  appLoading: boolean,
  accessToken: string | null;
  user: IUser | null,
}

export const useAppStore = defineStore("appStore", {
  state: (): IAppState => ({
    appLoading: true,
    user: null,
    accessToken: null,
  }),
  actions: {
    auth(accessToken: string, user: IUser) {
      this.accessToken = accessToken;
      this.user = user;
      setJwt(accessToken);
    },
    logout() {
      removeJwt();
      this.accessToken = null;
      this.user = null;
    },
    async initAuth() {
      try {
        const { accessToken, user } = await AuthService.updateTokens();
        this.auth(accessToken, user);
      } catch (e) {
        console.log(e);
        return Promise.reject();
      } finally {
        this.appLoading = false;
      }
    },
  },
});
