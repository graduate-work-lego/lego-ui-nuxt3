// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxt/ui",
    "nuxt-lodash",
    '@pinia/nuxt',
    '@nuxtjs/google-fonts'
  ],
  // todo: может получится использовать динамические иконки в шаблоне блоков
  // ui: {
  //   icons: {
  //     dynamic: true
  //   }
  // },
  app: {
    head: {
      title: "Конструктор сайтов",
    },
  },
  pinia: {
    storesDirs: ['./store/**'],
  },
  googleFonts: {
    preload: true,
    families: {
      Montserrat: '200..900',
      Roboto: '200..900',
      Inter: '200..900',
    }
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: "@use \"~/assets/styles/globals.scss\" as *;",
        },
      },
    },
  },
});
