export default defineAppConfig({
  ui: {
    primary: 'yellow',
    gray: 'cool',
    skeleton: {
      background: 'bg-gray-300 dark:bg-gray-800',
    }
  }
})